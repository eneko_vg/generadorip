import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GeneradorIptest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void testGenerarNumero() {
		assertEquals(0, 254,"Generaci�n de n�mero aleatorio");
	}
	
	@Test
	public void testGenerarIp() {
		assertEquals(0, 254, "Generaci�n de n�mero ip");
	}

}


import java.util.Random;

public class GeneradorIp {
	
	public int ip;
	public int generarNumero(int min, int max) {
		Random rnd = new Random();
			ip = rnd.nextInt()*(min-max+1)+max;
		return ip;
	}
	
	public String generarIp(){
		String red = "";
		for(int i=0; i<4; i++) {
			if (i < 4){
				red += ip + (".");
			}else {
				red += ip;
			}
		}
		return red;
	}
}
